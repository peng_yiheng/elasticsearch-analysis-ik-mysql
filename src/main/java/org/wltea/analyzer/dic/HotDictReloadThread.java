package org.wltea.analyzer.dic;

import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

/**
 * @Author sannian
 * @Date 2021/7/12 15:17
 * @Version 1.0
 */

public class HotDictReloadThread implements Runnable {

    private static final Logger logger = ESPluginLoggerFactory.getLogger(HotDictReloadThread.class.getName());

    @Override
    public void run() {
        while(true) {
            logger.info("[==========]reload hot dict from mysql......");
            Dictionary.getSingleton().reLoadMainDict();
            try {
                Thread.sleep( 10000L);//Long.parseLong(Config.get(Config.JDBC_RELOAD_INTERVAL))
            } catch (InterruptedException e) {
                try {
                    Thread.sleep(10000L);
                    //14400000L
                } catch (InterruptedException ex) {
                    logger.error(ex);
                }
            }
        }
    }

}
