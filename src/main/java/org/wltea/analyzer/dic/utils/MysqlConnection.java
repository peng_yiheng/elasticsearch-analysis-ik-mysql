package org.wltea.analyzer.dic.utils;

import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author DELL
 */
public class MysqlConnection {
    private static final Logger logger = ESPluginLoggerFactory.getLogger(MysqlConnection.class.getName());
    public static synchronized Connection getConnection() {
        Connection conn = null;
        try {
            // 动态加载mysql驱动
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://192.168.0.149:3306/data_flow?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8";
            String username ="dbflow";
            String password = "dbflowF4123";
            //String url = Config.get(Config.KEY_JDBC_MYSQL_URL);
            //String username = Config.get(Config.KEY_JDBC_MYSQL_USERNAME);
            //String password = Config.get(Config.KEY_JDBC_MYSQL_PASSWORD);
            logger.info("url: ->{}",url);
            logger.info("username: ->{}",username);
            logger.info("password: ->{}",password);
            conn = DriverManager.getConnection(url, username, password);
            logger.info("conn: ->{}",conn);
        } catch (Exception e) {
            logger.error(e);
        }
        return conn;
    }

    public static void close(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            logger.error(e);
        }
    }

}
