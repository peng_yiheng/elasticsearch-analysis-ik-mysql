package org.wltea.analyzer.dic.utils;

import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.dic.DictSegment;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * @author DELL
 */
public class MysqlUtils {
	private static final Logger logger = ESPluginLoggerFactory.getLogger(MysqlUtils.class.getName());
	public static final String sql = "SELECT * FROM ik_word";
	//public static final String sql = Config.get(Config.KEY_JDBC_MYSQL_SQL);

	public static void loadMySQLExtDict(DictSegment _MainDict) {
		Connection conn = MysqlConnection.getConnection();
		ResultSet rs = getResult(conn, sql);
		int count = 0;
		try {
			while(rs.next()) {
				String theWord = rs.getString("word");
				logger.info("[==========]hot word from mysql: " + theWord);
				_MainDict.fillSegment(theWord.trim().toCharArray());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(e.getMessage());
		}
		finally {
			logger.info("@@@Mysql===getEventExpressions==="+count);
			MysqlConnection.close(conn);
		}
	}

	private static ResultSet getResult(Connection conn, String sql) {
		ResultSet rs = null;
		PreparedStatement statement = null;
		try {
			statement = conn.prepareStatement(sql);
			rs = statement.executeQuery(sql);
		} catch (SQLException e) {
			logger.error(e);
		}
		return rs;
	}

}
