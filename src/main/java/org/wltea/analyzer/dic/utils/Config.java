package org.wltea.analyzer.dic.utils;

import org.apache.logging.log4j.Logger;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author DELL
 */
public class Config {
	public static final String KEY_JDBC_MYSQL_URL = "key.jdbc.mysql.url";
	public static final String KEY_JDBC_MYSQL_PASSWORD = "key.jdbc.mysql.password";
	public static final String KEY_JDBC_MYSQL_USERNAME = "key.jdbc.mysql.username";
	public static final String JDBC_RELOAD_INTERVAL = "jdbc.reload.interval";
	public static final String KEY_JDBC_MYSQL_SQL = "key.jdbc.mysql.sql";
	private static final Logger logger = ESPluginLoggerFactory.getLogger(Config.class.getName());

	static Properties pps = new Properties();

	static {
		try {

			InputStream input = Config.class.getClassLoader().getResourceAsStream("env.properties");

			if (input == null) {
				input = new FileInputStream(System.getProperty("user.dir") + "/config/env.properties");
			}

			pps.load(input);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static Map<String, Properties> props = new HashMap<>();

	public static String get(String prop, String key) {
		if (!props.containsKey(prop)) {
			loadProp(prop);
		}
		return props.get(prop).getProperty(key);
	}

	public static String get(String key) {
		return get("env", key);
	}

	private static void loadProp(String prop) {
		if (!props.containsKey(prop)) {
			synchronized (props) {
				if (!props.containsKey(prop)) {
					try {
						Properties p = new Properties();
						InputStream in;

						File file = new File(System.getProperty("user.dir") + "/config/" + prop + ".properties");

						if (file.exists()) {
							in = new FileInputStream(file);
						} else {
							in = Config.class.getResourceAsStream("/" + prop + ".properties");
						}
						p.load(new InputStreamReader(in, Charset.forName("utf-8")));
						props.put(prop, p);
					} catch (FileNotFoundException e) {
						logger.error("properties file {} not exist !", "/config/" + prop + ".properties", e);
					} catch (IOException e) {
						logger.error("load properties {} failed ! ", prop + ".properties", e);
					}
				}
			}
		}
	}

}
